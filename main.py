import pygame
import os
from config import *
import sys
import random 

os.environ['SDL_VIDEO_CENTERED'] = '1'
pygame.init()

screen = pygame.display.set_mode(SIZE, pygame.RESIZABLE)
pygame.display.set_caption("Flash card")
clock = pygame.time.Clock()
class Card():
    def __init__(self):
        
        self.text_color = BLACK
        self.box_color = BLUE
        self.box_color2 = DARKER_BLUE
        self.surface1 = pygame.Surface((WIDTH*0.65,HEIGHT*0.4),pygame.SRCALPHA)
        self.surface2 = pygame.Surface((WIDTH*0.65,HEIGHT*0.20),pygame.SRCALPHA)
        self.surface3 = pygame.Surface((WIDTH*0.65,30),pygame.SRCALPHA)
        self.surface1.fill(self.box_color)
        self.surface2.fill(self.box_color)
        self.surface3.fill(self.box_color)
        self.coords1 = (WIDTH//2-self.surface1.get_size()[0]//2,HEIGHT//2-self.surface1.get_size()[1]//2)
        self.coords2 = (WIDTH//2-self.surface2.get_size()[0]//2,HEIGHT//2-self.surface2.get_size()[1]//2)
        self.coords3 = (WIDTH//2-self.surface3.get_size()[0]//2,HEIGHT//2-self.surface3.get_size()[1]//2)
        self.state = 1
        self.num = True
        self.font = pygame.font.SysFont("sans-serif", int(self.surface1.get_size()[0]*0.1))
        self.font.set_bold(True)
        self.text1 = ""
        self.text2 = ""
        
        
    def draw(self, state):
        self.state = state
        if self.state == 1:
            screen.blit(self.surface1, self.coords1)
            if self.num:
                screen.blit(self.text_surface1, self.coords1)
            else: screen.blit(self.text_surface2, self.coords1)
            
        if self.state == 2:
            screen.blit(self.surface2, self.coords2)
        if self.state == 3:
            screen.blit(self.surface3, self.coords3)

    def on_mouse(self, mouse_coords):
        width = self.surface1.get_size()[0]
        height = self.surface1.get_size()[1]
        up_left_x = self.coords1[0]
        up_left_y = self.coords1[1]
        bootom_right_x = self.coords1[0]+width
        bottom_right_y = self.coords1[1]+height
        if up_left_x+(bootom_right_x-up_left_x) > mouse_coords[0] > up_left_x and up_left_y+(bottom_right_y-up_left_y) > mouse_coords[1] > up_left_y:
            self.surface1.fill(self.box_color2)
            return True
        else:
            self.surface1.fill(self.box_color)
        return False
    def draw_text(self, text1, text2):
        self.text1 = text1
        self.text2 = text2
        if self.state == 1:
            self.text_surface1 = pygame.Surface(self.surface1.get_size(),pygame.SRCALPHA)
            self.render_text = self.font.render(self.text1, True, self.text_color)
            self.text_surface1.blit(self.render_text, (self.text_surface1.get_size()[0]//2-self.render_text.get_size()[0]//2,self.text_surface1.get_size()[1]//2-self.render_text.get_size()[1]//2))
            self.text_surface2 = pygame.Surface(self.surface1.get_size(),pygame.SRCALPHA)
            self.render_text2 = self.font.render(self.text2, True, self.text_color)
            self.text_surface2.blit(self.render_text2, (self.text_surface2.get_size()[0]//2-self.render_text2.get_size()[0]//2,self.text_surface2.get_size()[1]//2-self.render_text2.get_size()[1]//2))

    def set_text(self, num):
        self.num = num

    def change_size(self):
        self.surface1 = pygame.Surface((WIDTH*0.65,HEIGHT*0.4),pygame.SRCALPHA)
        self.surface2 = pygame.Surface((WIDTH*0.65,HEIGHT*0.20),pygame.SRCALPHA)
        self.surface3 = pygame.Surface((WIDTH*0.65,30),pygame.SRCALPHA)
        self.surface1.fill(self.box_color)
        self.surface2.fill(self.box_color)
        self.surface3.fill(self.box_color)
        self.coords1 = (WIDTH//2-self.surface1.get_size()[0]//2,HEIGHT//2-self.surface1.get_size()[1]//2)
        self.coords2 = (WIDTH//2-self.surface2.get_size()[0]//2,HEIGHT//2-self.surface2.get_size()[1]//2)
        self.coords3 = (WIDTH//2-self.surface3.get_size()[0]//2,HEIGHT//2-self.surface3.get_size()[1]//2)
        self.font = pygame.font.SysFont("sans-serif", int(self.surface1.get_size()[0]*0.1))
        self.font.set_bold(True)
        self.text_surface1 = pygame.Surface(self.surface1.get_size(),pygame.SRCALPHA)
        self.render_text = self.font.render(self.text1, True, self.text_color)
        self.text_surface1.blit(self.render_text, (self.text_surface1.get_size()[0]//2-self.render_text.get_size()[0]//2,self.text_surface1.get_size()[1]//2-self.render_text.get_size()[1]//2))
        self.text_surface2 = pygame.Surface(self.surface1.get_size(),pygame.SRCALPHA)
        self.render_text2 = self.font.render(self.text2, True, self.text_color)
        self.text_surface2.blit(self.render_text2, (self.text_surface2.get_size()[0]//2-self.render_text2.get_size()[0]//2,self.text_surface2.get_size()[1]//2-self.render_text2.get_size()[1]//2))


card = Card()
word = random.choice(words)
card.draw_text(word["a"],word["b"])
state = 1
timer = 0
start_timer = False
text_num = True

class Next_btn():
    def __init__(self):
        self.box_color = GREEN
        self.box_color2 = DARKER_GREEN
        self.text = "NEXT"
        self.text_color = BLACK
        self.surface = pygame.Surface((card.surface1.get_size()[0]*0.3,card.surface1.get_size()[1]*0.3), pygame.SRCALPHA)
        self.surface.fill(self.box_color)
        self.coords = 0,0
        
        self.font = pygame.font.SysFont("sans-serif", int(card.surface1.get_size()[0]*0.1))
        self.font.set_bold(True)
        self.draw_text(self.text)
    def on_mouse(self, mouse_coords):
        width = self.surface.get_size()[0]
        height = self.surface.get_size()[1]
        up_left_x = self.coords[0]
        up_left_y = self.coords[1]
        bootom_right_x = self.coords[0]+width
        bottom_right_y = self.coords[1]+height
        if up_left_x+(bootom_right_x-up_left_x) > mouse_coords[0] > up_left_x and up_left_y+(bottom_right_y-up_left_y) > mouse_coords[1] > up_left_y:
            self.surface.fill(self.box_color2)
            return True
        else:
            self.surface.fill(self.box_color)
        return False

    def draw(self):
        screen.blit(self.surface,self.coords)
        screen.blit(self.text_surface, self.coords)
    def draw_text(self, text):
        self.text = text
        self.text_surface = pygame.Surface(self.surface.get_size(),pygame.SRCALPHA)
        self.render_text = self.font.render(text, True, self.text_color)
        self.text_surface.blit(self.render_text, (self.text_surface.get_size()[0]//2-self.render_text.get_size()[0]//2,self.text_surface.get_size()[1]//2-self.render_text.get_size()[1]//2))

    def change_size(self):
        self.surface = pygame.Surface((card.surface1.get_size()[0]*0.3,card.surface1.get_size()[1]*0.3), pygame.SRCALPHA)
        self.surface.fill(self.box_color)
        self.font = pygame.font.SysFont("sans-serif", int(card.surface1.get_size()[0]*0.1))
        self.font.set_bold(True)
        self.text_surface = pygame.Surface(self.surface.get_size(),pygame.SRCALPHA)
        self.render_text = self.font.render(self.text, True, self.text_color)
        self.text_surface.blit(self.render_text, (self.text_surface.get_size()[0]//2-self.render_text.get_size()[0]//2,self.text_surface.get_size()[1]//2-self.render_text.get_size()[1]//2))

class Menu_btn():
    def __init__(self):
        self.box_color = GREEN
        self.box_color2 = DARKER_GREEN
        self.text = "MENU"
        self.text_color = BLACK
        self.surface = pygame.Surface((card.surface1.get_size()[0]*0.3,card.surface1.get_size()[1]*0.3), pygame.SRCALPHA)
        self.surface.fill(self.box_color)
        self.coords = WIDTH-self.surface.get_size()[0], 0
        self.font = pygame.font.SysFont("sans-serif", int(card.surface1.get_size()[0]*0.1))
        self.font.set_bold(True)
        self.draw_text(self.text)
        self.active = False
    def on_mouse(self, mouse_coords):
        width = self.surface.get_size()[0]
        height = self.surface.get_size()[1]
        up_left_x = self.coords[0]
        up_left_y = self.coords[1]
        bootom_right_x = self.coords[0]+width
        bottom_right_y = self.coords[1]+height
        if up_left_x+(bootom_right_x-up_left_x) > mouse_coords[0] > up_left_x and up_left_y+(bottom_right_y-up_left_y) > mouse_coords[1] > up_left_y:
            self.surface.fill(self.box_color2)
            return True
        else:
            self.surface.fill(self.box_color)
        return False

    def draw(self):
        screen.blit(self.surface,self.coords)
        screen.blit(self.text_surface, self.coords)
    def draw_text(self, text):
        self.text = text
        self.text_surface = pygame.Surface(self.surface.get_size(),pygame.SRCALPHA)
        self.render_text = self.font.render(self.text, True, self.text_color)
        self.text_surface.blit(self.render_text, (self.text_surface.get_size()[0]//2-self.render_text.get_size()[0]//2,self.text_surface.get_size()[1]//2-self.render_text.get_size()[1]//2))

    def change_size(self):
        self.surface = pygame.Surface((card.surface1.get_size()[0]*0.3,card.surface1.get_size()[1]*0.3), pygame.SRCALPHA)
        self.surface.fill(self.box_color)
        self.font = pygame.font.SysFont("sans-serif", int(card.surface1.get_size()[0]*0.1))
        self.font.set_bold(True)
        self.coords = WIDTH-self.surface.get_size()[0], 0
        self.text_surface = pygame.Surface(self.surface.get_size(),pygame.SRCALPHA)
        self.render_text = self.font.render(self.text, True, self.text_color)
        self.text_surface.blit(self.render_text, (self.text_surface.get_size()[0]//2-self.render_text.get_size()[0]//2,self.text_surface.get_size()[1]//2-self.render_text.get_size()[1]//2))

next_btn = Next_btn()
menu_btn = Menu_btn()
clicked = False
new_text = ""

class Types():
    def __init__(self,type_name, type_text, coords):
        self.type_name = type_name
        self.type_text = type_text
        self.coords = coords
        self.text_color = BLACK
        self.box_color = GREEN
        self.box_color2 = DARKER_GREEN
        self.font = pygame.font.SysFont("sans-serif", int(card.surface1.get_size()[0]*0.07))
        self.font.set_bold(True)
        self.render_text = self.font.render(self.type_text, True, self.text_color)
        self.text_surface = pygame.Surface(self.render_text.get_size(), pygame.SRCALPHA)
        self.text_surface.fill(self.box_color)
        self.text_surface.blit(self.render_text, (self.text_surface.get_size()[0]//2-self.render_text.get_size()[0]//2,self.text_surface.get_size()[1]//2-self.render_text.get_size()[1]//2))

    def draw(self):
        screen.blit(self.text_surface, self.coords)
    def on_mouse(self, mouse_coords):
        width = self.text_surface.get_size()[0]
        height = self.text_surface.get_size()[1]
        up_left_x = self.coords[0]
        up_left_y = self.coords[1]
        bootom_right_x = self.coords[0]+width
        bottom_right_y = self.coords[1]+height
        if up_left_x+(bootom_right_x-up_left_x) > mouse_coords[0] > up_left_x and up_left_y+(bottom_right_y-up_left_y) > mouse_coords[1] > up_left_y:
            self.text_surface.fill(self.box_color2)
            self.text_surface.blit(self.render_text, (self.text_surface.get_size()[0]//2-self.render_text.get_size()[0]//2,self.text_surface.get_size()[1]//2-self.render_text.get_size()[1]//2))
            return True
        else:
            self.text_surface.fill(self.box_color)
            self.text_surface.blit(self.render_text, (self.text_surface.get_size()[0]//2-self.render_text.get_size()[0]//2,self.text_surface.get_size()[1]//2-self.render_text.get_size()[1]//2))
        return False

    def change_size(self):
        self.font = pygame.font.SysFont("sans-serif", int(card.surface1.get_size()[0]*0.07))
        self.font.set_bold(True)
        self.render_text = self.font.render(self.type_text, True, self.text_color)
        self.text_surface = pygame.Surface(self.render_text.get_size(), pygame.SRCALPHA)
        self.text_surface.fill(self.box_color)
        self.text_surface.blit(self.render_text, (self.text_surface.get_size()[0]//2-self.render_text.get_size()[0]//2,self.text_surface.get_size()[1]//2-self.render_text.get_size()[1]//2))

word_types = sorted(set([x["type"] for x in words]))

word_types.insert(0,"any")

type_list = []
x_axis = 10
y_axis = 10
longest_type = 0
for index, x in enumerate(word_types):
    if x != "any":
        temp_type = Types(x,f"{x}({len([y for y in words  if y['type'] == x ])})", (x_axis,y_axis))
    else:
        temp_type = Types(x,f"{x}({len(words)})", (x_axis,y_axis))
    type_list.append(temp_type)
    y_axis = temp_type.coords[1]+temp_type.text_surface.get_size()[1]*1.2
    type_width = temp_type.text_surface.get_size()[0]
    if type_width > longest_type: longest_type = type_width
    if index % 8 == 0 and index != 0:
        x_axis += longest_type*1.2
        y_axis = 10


selected_type = "any"
def changed_window(new):
    global OLD_SIZE
    if OLD_SIZE != new:
        OLD_SIZE = new
        return True
    return False
while True:
    screen.fill(BACKGROUND)
    if changed_window(screen.get_size()):

        SIZE = WIDTH, HEIGHT = screen.get_size()
        card.change_size()
        next_btn.change_size()
        x_axis = 10
        y_axis = 10
        longest_type = 0
        for index, word_type in enumerate(type_list):

            word_type.change_size()
                
            word_type.coords = (x_axis, y_axis)
            y_axis = word_type.coords[1]+word_type.text_surface.get_size()[1]*1.2
            type_width = word_type.text_surface.get_size()[0]
            if type_width > longest_type: longest_type = type_width
            if index % 8 == 0 and index != 0:
                x_axis += longest_type*1.2
                y_axis = 10
            
        menu_btn.change_size()
        

    
    for event in pygame.event.get():
        if event.type == pygame.QUIT: sys.exit()
        if event.type == pygame.KEYDOWN:
                
            if event.key == pygame.K_SPACE or event.key == pygame.K_RETURN or event.key == pygame.K_KP_ENTER and not menu_btn.active :
                start_timer = True
            if event.key == pygame.K_RIGHT or event.key == pygame.K_d and not menu_btn.active:
                text_num = True
                card.set_text(text_num)
                if selected_type == "any":
                    word = random.choice(words)
                else:
                    
                    word = random.choice([x for x in words if x["type"] == selected_type])
                card.draw_text(word["a"],word["b"])
 
    if start_timer:
        timer += 1
        if timer == 10:
            state = 2
        elif timer == 20:
            state = 3
        elif timer == 30:
            state = 2
        elif timer >= 40:
            state = 1
            timer = 0
            start_timer = False
            text_num = not text_num
            card.set_text(text_num)
    if state == 1  and card.on_mouse(pygame.mouse.get_pos()) and pygame.mouse.get_pressed()[0] == 1 and menu_btn.active == False:
        start_timer = True
    if next_btn.on_mouse(pygame.mouse.get_pos()) and pygame.mouse.get_pressed()[0] == 1 and not clicked and menu_btn.active == False:
        clicked = True
        text_num = True
        card.set_text(text_num)
        if selected_type == "any":
            word = random.choice(words)
        else:
            word = random.choice([x for x in words if x["type"] == selected_type])
        card.draw_text(word["a"],word["b"])

    if menu_btn.on_mouse(pygame.mouse.get_pos()) and pygame.mouse.get_pressed()[0] == 1 and not clicked:
        clicked = True
        menu_btn.active = not menu_btn.active
    for word_type in type_list:
        if menu_btn.active and not clicked and word_type.on_mouse(pygame.mouse.get_pos()) and pygame.mouse.get_pressed()[0] == 1:
            clicked = True
            selected_type = word_type.type_name
            menu_btn.active = False
            text_num = True
            card.set_text(text_num)
            word_type.on_mouse((-1,-1))
            if selected_type == "any":
                word = random.choice(words)
            else:
                word = random.choice([x for x in words if x["type"] == selected_type])
            card.draw_text(word["a"],word["b"])
    if pygame.mouse.get_pressed()[0] == 0 and clicked:
        clicked = False
    if not menu_btn.active:
        next_btn.draw()
    
        card.draw(state)
    else:
        for word_type in type_list:
            word_type.draw()

    menu_btn.draw()
    
    pygame.display.update()
    clock.tick(60)