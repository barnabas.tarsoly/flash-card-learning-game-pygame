Python Flash Card learning app

Useful informations: 
- Default window size: 1200x800, but resizeable
- The words.txt should look like this: word1 - word2 - category (optional)
- word1 and word2 max lengths are 22 character
- category max length is 10 character and max 27 character + "any" and "other"

